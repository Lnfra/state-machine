package com.example.fsmswitch;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;

class IO {
  BufferedReader br;

  public IO() {
    this.br = new BufferedReader(new InputStreamReader(System.in));
  }

  void print(String message) {
    System.out.println(message);
  }

  public String readLine() throws IOException {
    return br.readLine();
  }

  String readStringInput() {
    while (true) {
      try {
        return readLine();
      } catch (IOException e) {
        print("Unable to read input");
      }
    }
  }
}
