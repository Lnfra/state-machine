package com.example.fsmswitch;

enum State {
  Init, ShowMenu, ShowMsg, DisplayVideo, Invalid, Quit
}
