package com.example.fsmswitch;

import java.util.HashMap;
import java.util.Map;

import static com.example.fsmswitch.State.*;

//Represents the interface where users can execute commands by choosing an option
class Menu {
  private final IO io;

  //holds the state machine is currently in (enum or command object)
  State currentState;

  //Stores the behaviour of the machine
  Map<State, Map<String, State>> transitionTable;

  public Menu(IO io) {
    this.io = io;
    this.currentState = Init;
    transitionTable = createTable();
  }

  Map<State, Map<String, State>> createTable() {
    //Create possible transitions from Init State
    //At state Init, if user enters "" go to ShowMenu
    Map<String, State> transitionFromInit = new HashMap<>();
    transitionFromInit.put("", ShowMenu);

    //Create possible transitions from ShowMenu State
    Map<String, State> transitionFromShowMenu;
    transitionFromShowMenu = new HashMap<>();
    transitionFromShowMenu.put("1", ShowMsg);
    transitionFromShowMenu.put("2", DisplayVideo);
    transitionFromShowMenu.put("q", Quit);

    //Create possible transitions from ShowMsg State
    Map<String, State> transitionFromShowMsg = new HashMap<>();
    transitionFromShowMsg.put("b", ShowMenu);
    transitionFromShowMsg.put("q", Quit);

    //Create possible transitions from DisplayVideo State
    Map<String, State> transitionFromDisplayVideo = new HashMap<>();
    transitionFromDisplayVideo.put("b", ShowMenu);
    transitionFromDisplayVideo.put("q", Quit);

    //Create transitions from Invalid State
    Map<String, State> transitionFromInvalid = new HashMap<>();
    transitionFromInvalid.put("skip", ShowMenu);

    //Store all the above transitions into a table
    Map<State, Map<String, State>> table = new HashMap<>();
    table.put(Init, transitionFromInit);
    table.put(ShowMenu, transitionFromShowMenu);
    table.put(ShowMsg, transitionFromShowMsg);
    table.put(DisplayVideo, transitionFromDisplayVideo);
    table.put(Invalid, transitionFromInvalid);
    return table;
  }

  void run() {
    while (currentState != Quit) {
      switch (currentState) {

        case Init:
          Map<String, State> initPageChoices = transitionTable.get(currentState);

          //Do the stuff this state does eg print menu
          io.print("Welcome to the system");
          io.print("Press enter for main menu");

          //based on user input go to next state
          currentState = initPageChoices.getOrDefault(io.readStringInput(), Invalid);
          break;

        case ShowMenu:
          //get the transitions for this state
          Map<String, State> showMenuChoices = transitionTable.get(currentState);

          //Do the stuff this state does eg print menu
          io.print("Menu Options");
          io.print("1. Display Quote Of The Day");
          io.print("2. Display a random video");
          io.print("Press q to quit");

          //based on user input go to next state
          currentState = showMenuChoices.getOrDefault(io.readStringInput(), Invalid);
          break;

        case ShowMsg:
          Map<String, State> showMsgChoices = transitionTable.get(currentState);
          io.print("Today is your lucky day!");
          io.print("Press b to go back");
          io.print("Press q to quit");
          currentState = showMsgChoices.getOrDefault(io.readStringInput(), Invalid);
          break;

        case DisplayVideo:
          Map<String, State> displayVideoChoices = transitionTable.get(currentState);
          io.print("Playing video now.....");
          io.print("Sorry internet connection is down today.....");
          io.print("Press b to go back");
          io.print("Press q to quit");
          currentState = displayVideoChoices.getOrDefault(io.readStringInput(), Invalid);
          break;

        case Invalid:
          //Invalid state handles the error conditions
          Map<String, State> invalidChoices = transitionTable.get(currentState);
          io.print("You have entered an invalid choice");
          currentState = invalidChoices.get("skip");
          break;
      }
    }
  }
}

