package com.example.fsmswitch;

class Runner {
  public static void main(String[] args) {
    IO io = new IO();
    Menu menu = new Menu(io);
    menu.run();
  }
}

